# � 2019 TP
# AC - L4
.data
	MAX_LEN = 256
	msg_request_input:
		.asciiz "Enter string: "
	msg_describe_output:
		.asciiz "Result : "
	msg_len_output:
		.asciiz "String len: "
.text
	dispstrrev:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $fp, 0($sp)
		addi $fp, $sp, 8
		
		lw $t0, 0($fp)
		
		lb $t1, 0($t0) #incarc caracterul curent (asa zis primul)
		
		beq $t1, $zero, dispstrrev_done #verific conditie terminare (sir vid)
		
			addi $t0, $t0, 1	#Urmatoare subproblema: stringul care incepe cu car urmator
			addi $sp, $sp, -4	# spatiu pe stiva pt parametrul funtie len
			sw $t0, 0($sp)		# pun parametrul pe stiva
			
			jal dispstrrev				# apel recursiv
			
			addi $sp, $sp, 4	# eliberez stiva de parametru
			
			lw $t0, 0($fp)		
			lb $a0, 0($t0) #incarc din nou caracterul curent pentru ca apelul recursiv mi l-a suprascris
			li $v0, 11
			syscall
			
		dispstrrev_done:
			lw $ra, 4($sp)
			lw $fp, 0($sp)
			addi $sp, $sp, 8
			jr $ra
		
		
	strlen:
		addi $sp, $sp, -8
		sw $ra, 4($sp)
		sw $fp, 0($sp)
		addi $fp, $sp, 8
		
		lw $t0, 0($fp)
		
		lb $t1, 0($t0) #incarc caracterul curent (asa zis primul)
		
		bne $t1, $zero, stepin #verific conditie terminare (sir vid)
		xor $v0, $v0, $v0		# sir vid => lungime 0 => v0=0
		
		j done
		stepin:
			addi $t0, $t0, 1	#Urmatoare subproblema: stringul care incepe cu car urmator
			addi $sp, $sp, -4	# spatiu pe stiva pt parametrul funtie len
			sw $t0, 0($sp)		# pun parametrul pe stiva
			
			jal strlen				# apel recursiv
			
			addi $sp, $sp, 4	# eliberez stiva de parametru
			addi $v0, $v0, 1	
			
		done:
			lw $ra, 4($sp)
			lw $fp, 0($sp)
			addi $sp, $sp, 8
			jr $ra
		
	main:
		addi $sp, $sp, -8			# am pregatit context
		sw $ra, 4($sp)
		sw $fp, 0($sp)
		addi $fp, $sp, 8
		
		la $a0, msg_request_input	# afisez cererea pt util.
		addi $v0, $zero, 4
		syscall
		
		li $a1, MAX_LEN	
		sub $sp, $sp, $a1
		addi $a0, $sp, 0
		addi $v0, $zero, 8
		syscall
		
		# apel rezolvari lab 4
		
		addi $t0, $sp, 0	# adr stringului de pe stiva...
		addi $sp, $sp, -4	# spatiu pt parametrul transmis
		sw $t0, 0($sp)
		
		jal strlen			# len primeste pe stiva pointer la sir
		
		
		sw $v0, 0($sp)	# optimizez (nu mai dealoc si realoc pe stiva)
		
		la $a0, msg_len_output
		addi $v0, $zero, 4
		syscall		
		
		lw $a0, 0($sp)	# intregul de afisat		
		addi $sp, $sp, 4	#free stack
		
		addi $v0, $zero, 1 # codul afisarii de intreg
		syscall
		
		addi $v0, $zero, 11 #trec pe linie noua
		addi $a0, $zero, 10
		syscall
		
		# sfarsit apel rez lab 4
		
		la $a0, msg_describe_output
		addi $v0, $zero, 4
		syscall
		
		addi $t0, $sp, 0	# adr stringului de pe stiva...
		addi $sp, $sp, -4	# spatiu pt parametrul transmis
		sw $t0, 0($sp)
		
		jal dispstrrev			# len primeste pe stiva pointer la sir
		
		addi $sp, $sp, 4	# eliberez stiva de parametrul trimis
		
		addi $v0, $zero, 11 #trec pe linie noua
		addi $a0, $zero, 10
		syscall
		
		
		la $a0, msg_describe_output
		addi $v0, $zero, 4
		syscall
		
		addi $a0, $sp, 0
		addi $v0, $zero, 4
		syscall
		
		li $t0, MAX_LEN				# dealocare string local
		add $sp, $sp, $t0
		
		lw $ra 4($sp)				#restaurare context
		lw $fp 0($sp)
		addi $sp, $sp, 8
		jr $ra						# iesire din f. main
	.end main