# WARNING: QTSpim simulator is not a "cycle accurate" simulator
#            and generates jump offsets ignoring the PC<=PC+4+offset
# SOLUTION: as a result all jump offsets need subtracting one unit
#            before loading in code memory

init:
add $t1, $zero, $zero	#counter init (<init)
addi $t0, $zero, 64		#data segment pointer
addi $t2, $zero, 3		#set event value
sw $t2, 4($t0)			#put event value in memory

again:	
sw $t1, 0($t0)			#store counter in memory (<again)
lw $t2, 4($t0)			#load event value from memory
addi $t1, $t1, 1		#count in $t1
beq $t1, $t2, event		#testif counter reached event
bne $t1, $t2, again		#just loop again

event:	
ori $t0, $t0, 0			#unsupported operation (<event)
beq $t1, $t2, again		#loop to again
