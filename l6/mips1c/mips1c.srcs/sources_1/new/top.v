`timescale 1ns / 1ps

module enabler(input clk, input res, input step, output out);
    reg [1:0] state;
    always@(posedge clk)
        if (res)
            state<=0;
        else
            casex({state,step})
                3'b001: state<=2; //when detecting step rising edge, transition to state 2
                3'b101: state<=1; //if step is still active, transition to state 1
                3'b100: state<=0; //if step became inactive, transition to state 0
                3'b010: state<=0; //when detecting step falling edge
                3'b11x: state<=0; //unused state code, always transition to state 0 
                default: state<=state;
            endcase
     assign out=state[1];
endmodule

module top(
    output [3:0] led,
    input [3:0] btn,
    input CLK100MHZ
    );
    
    reg [3:0] btn_buf; //buttons sampling buffer (to avoid metastability, see Digital System Design)
    wire en;           //enabler->cpu signal net
    wire [31:0] data;  //cpu->led net
    
    //sampling button states to button buffer registers to avoid metastability
    always@(posedge CLK100MHZ)
        btn_buf<=btn;
    
    //workaround device to allow single step debugging using a board button     
    enabler en0(.clk(CLK100MHZ), .res(btn_buf[1]), .step(btn_buf[0]), .out(en));
    
    //cpu instance connected to the enabler and the board i/o     
    cpu cpu0(.clk(CLK100MHZ), .en(en|btn_buf[2]), .res(btn_buf[1]), .dout(data));
    
    //displaying cpu output on leds (Program Counter devided by 4)
    assign led = data[5:2];            
endmodule
