`timescale 1ns / 1ps

`define RTYPE   6'b000000    
`define ADDI    6'b001000    
`define LW      6'b100011
`define SW      6'b101011
`define BEQ     6'b000100
`define BNE     6'b000101
`define BEQ_BNE 6'b00010x

`define ALU_ADD 4'b0010
`define ALU_SUB 4'b0110
`define ALU_AND 4'b0000
`define ALU_OR  4'b0001
`define ALU_SLT 4'b0111
`define ALU_NOR 4'b1100

`define REG_t0  5'b01000
`define REG_t1  5'b01001
`define REG_t2  5'b01010
`define REG_t3  5'b01011

module cpu(
    clk,
    res,
    en,
    dout
    );
    
    // the most important signals
    input           clk;
    input           res;
    input en;
    
    // some output - maybe it will go to the digits or leds
    output wire [31:0]   dout;
    
    // storage/state elements of the CPU
    reg [31:0] registerFile [31:0];
    reg [31:0] dataMemory   [0:63];
    reg [31:0] codeMemory   [0:255];

    // program counter register
    reg [31:0] PC;
    
    // the number of accessed register form register file
    wire [4:0] ra;
    wire [4:0] rb;
    wire [4:0] rc;
   
    // the data read from two registers 
    wire [31:0] da;
    wire [31:0] db;
    
    // the data that is written in the third register
    wire [31:0] dc;
    
    // the 16 bit immediate field from the instruction word
    wire [15:0] imm16;
    
    // the 32 bit sign extension of the immediate field
    wire [31:0] imm32;
    
    // shift amount field from the instruction word
    wire [4:0] shamt;
    
    // function field from the instruction word
    wire [5:0] funct;
    
    // opcode field form the instruction word
    wire [5:0] opcode;
    
    // the two ALU inputs
    wire [31:0] ALUA;
    wire [31:0] ALUB;
    
    // the ALU output
    reg [31:0] ALUOut;
    
    // the ALU function code: specifies what function the ALU performs on its inputs
    reg [3:0] ALUCode;
    
    // the status output from the ALU - set if the ALU result is 0 
    wire ZERO;
    
    // control signals - as presented in H&P Book
    reg RegWrite;
    reg MemRead;
    reg MemWrite;
    reg ALUSrc;
    reg RegDst;
    reg MemtoReg;
    reg Branch;
    reg [1:0] ALUOp;
    
    // the current instruction extraced from code memory
    wire [31:0] instruction;
    
    // some wires used with the data memory 
    wire [31:0] dataValueIn;
    wire [31:0] dataValueOut;
  
    // register file behavior: read operation
    assign da = (ra!=0) ? registerFile[ra] : 0;
    assign db = (rb!=0) ? registerFile[rb] : 0; 
    
    // register file behavior: write operation
        
        // select the value that is written into register file
    assign dc = (MemtoReg == 1) ? dataValueOut : ALUOut;
    
        // the actual writing is described here
    always@(posedge clk) begin
        if (en) if (RegWrite == 1'b1) begin
            // the destination register selection is decribed here "inline"/"inplace"
            registerFile[(RegDst == 1) ? rc : rb] <= dc;
        end
    end
    
    // code memory behavior: read the current instruction
         // TBD solve this memory addressing!
    assign instruction = codeMemory[PC>>2];
     
        // IMPORTANT: used only in simulation to initialize the code memory content
    initial $readmemh("code.mem", codeMemory, 0);
    
    // instruction fields - commodity/readability stuff
    assign opcode   = instruction[31:26];
    assign ra       = instruction[25:21];
    assign rb       = instruction[20:16];
    assign rc       = instruction[15:11];
    assign shamt    = instruction[10:6];
    assign funct    = instruction[5:0];
    assign imm16    = instruction[15:0];
    assign imm32    = {{16{imm16[15]}},imm16};
        
    // data memory behavior: read operation
        
        // TBD solve this memory addressing!
    assign dataValueOut = dataMemory[ALUOut>>2]; 
    
    // data memory behavior: write operation
    always@(posedge clk) begin
        if (en) if (MemWrite == 1'b1) begin
            // TBD solve this memory addressing!
            dataMemory[ALUOut>>2]      <= db;
        end
    end
    
    // PC register behavior
    always@(posedge clk) begin
        if (res == 1) begin
            PC <= 0;
        end else if(en) begin
            // TBD Solve this issue: it fails on timing for 100 MHz clock!
            if (((opcode == `BNE) && (ZERO != 1))
              ||((opcode == `BEQ) && (ZERO == 1))) begin
                // IMPORTANT: this line of code sets how the imm field is computed/stored for/into branch instructions!  
                PC <= PC + 4 + (imm32 << 2);
            end else begin
                PC <= PC + 4;
            end
        end
    end
    
    // ALU behavior
    
        // commodity/readability assignments for ALU inputs
    assign ALUA = da;
    assign ALUB = (ALUSrc == 1) ? imm32 : db;
    
        // ALU function decoding
    always@(ALUOp or funct) begin
        casex ({ALUOp, funct})
            8'b00_xxxxxx:   ALUCode = `ALU_ADD;
            8'b01_xxxxxx:   ALUCode = `ALU_SUB;
            8'b10_100000:   ALUCode = `ALU_ADD;
            8'b10_100010:   ALUCode = `ALU_SUB;
            8'b10_100100:   ALUCode = `ALU_AND;
            8'b10_100101:   ALUCode = `ALU_OR;
            8'b10_101010:   ALUCode = `ALU_SLT;
            default:        ALUCode = 4'b1111; 
        endcase
    end
    
        // ALU effective operations
    always@(ALUCode or ALUA or ALUB) begin
        case (ALUCode) 
            `ALU_AND:    ALUOut = ALUA & ALUB;
            `ALU_OR :    ALUOut = ALUA | ALUB;
            `ALU_ADD:    ALUOut = ALUA + ALUB;
            `ALU_SUB:    ALUOut = ALUA - ALUB;
            `ALU_SLT:    ALUOut = (ALUA < ALUB) ? 1 : 0;
            `ALU_NOR:    ALUOut = ~(ALUA | ALUB);
            default:    ALUOut = 0;
        endcase
    end
    
        // status output of the ALU
    assign ZERO = (ALUOut == 0);
    
    // CPU control behavior
    always@(opcode) begin
        casex (opcode) 
            `RTYPE  : {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'b100100010;
            `LW     : {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'b011110000;
            `SW     : {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'bx1x0__010_00;
            `BEQ_BNE: {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'bx0x0__000_01;
            `ADDI   : {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'b010100000;
            default : {RegDst, ALUSrc, MemtoReg, RegWrite, 
                        MemRead, MemWrite, Branch, ALUOp} = 9'b000000000;
        endcase
    end
    assign dout = PC;     
endmodule
