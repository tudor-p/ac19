`timescale 1ns / 1ps

`define STP #10
module tb_top();
    reg clk;
    reg [3:0] btn;
    wire [3:0] led;
    integer i;
    top top0(.CLK100MHZ(clk), .btn(btn), .led(led));
    initial
        forever #1 clk<=~clk;
    initial begin
        #0 clk=0; btn=2;
        `STP btn=0;        
        for(i=0;i<128;i=i+1) begin
            `STP btn=1; `STP btn=0;
        end
        #1 btn=4;
        //#100 btn=0;
        #10 $finish;
    end
endmodule
