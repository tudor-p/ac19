`timescale 1ns / 1ps

module tb_cpu();
reg clk;
reg res;
reg en;
wire [31:0] dout;
cpu cpu0(.clk(clk), .res(res), .dout(dout), .en(en));
initial
    forever #10 clk<=~clk;
initial 
    begin
        clk=0;
        res=0;
        en=0;
        #25 res=1;
        #30 res=0;
        #30 en=1;
        #1000 $finish;
    end
endmodule
